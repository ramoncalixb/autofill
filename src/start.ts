import app from '@server';
import { logger } from '@shared';
import { createConnection } from "typeorm";

createConnection({
    type: "sqlite",
    database: "test",
    entities: [
        __dirname + "/entities/*"
    ],
    synchronize: true,
    logging: false
})
.then(async connection => {
// Start the server
const port = Number(process.env.PORT || 3000);
app.listen(port, () => {
    logger.info('Express server started on port: ' + port);
});
})
.catch((error) => console.log(error));