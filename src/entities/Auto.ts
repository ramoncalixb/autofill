import {Entity, PrimaryGeneratedColumn, Column} from "typeorm";

@Entity()
export class Auto {
  @PrimaryGeneratedColumn()
  public id!: number;

  @Column()
  public uuid!: number;

  @Column()
  public vin!: string;

  @Column()
  public make!: string;

  @Column()
  public model!: string;

  @Column()
  public mileage!: string;

  @Column()
  public year!: string;

  @Column()
  public price!: string;

  @Column()
  public zipCode!: string;

  @Column()
  public createDate!: string;

  @Column()
  public updateDate!: string;
}