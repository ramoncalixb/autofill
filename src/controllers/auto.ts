import {Request, Response} from "express";
import {getManager, Repository} from "typeorm";
import csv from 'csvtojson';
import {Auto} from '../entities';
import { IncomingForm } from 'formidable';

export async function autoReader(request: Request, response: Response) {
  const form = new IncomingForm(),
  files: any = [],
  fields: any = [];
  let jsonArray;
  form
  .on('field', (field, value) =>{
    fields.push([field, value])
  })
  .on('file',  (field, file) => {
    files.push(file)
  })
  form.on('end', async() => {
    jsonArray= await csv().fromFile(files[0].path);
    const AutoRepository = getManager().getRepository(Auto);
    jsonArray.forEach(async(row: any) => {
        const autoFi = new Auto();
        autoFi.uuid = row.UUID;
        autoFi.vin = row.VIN;
        autoFi.make = row.Make;
        autoFi.model = row.Model;
        autoFi.mileage = row.Mileage;
        autoFi.year= row.Year;
        autoFi.price = row.Price;
        autoFi.createDate = row.CreateDate;
        autoFi.updateDate = row.UpdateDate
        autoFi.zipCode = row.zipCode;
        await AutoRepository.save(autoFi).then((response) => {
          console.log(response)
        })
    });
    const records = await AutoRepository.find();
    response.json(records)
  })
  form.parse(request)

}