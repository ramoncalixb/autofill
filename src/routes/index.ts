import { Router } from 'express';
// import UserRouter from './Users';
import { Request, Response, Express } from 'express';
import { autoReader } from '../controllers/Auto';
// Init router and path
const router = Router();

router.post('/reader', autoReader);
// Export the base-router
export default router;
