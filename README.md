# AutoFill Project


This project is a template and example to manage a CSV to json using SQLite


# New Features!

  - Uploa csv document
  - Save on json structure in Sqlite
  - It's free

> The Project was made using a node with typescript 
> with SQLite like database 
> the CSV was parsed using csvtojson
> to handle any possible CSV format (/,-,:,*)


### Tech

* [formidable] - Handle documents and stream files
* [csvtojson] - For convert csv to json format
* [typeorm] - is an ORM that can run in NodeJS


### Installation

Install the dependencies and devDependencies and start the server.

```sh
$ cd AutoFi
$ npm install 
$ npm run start-dev
```

How to called from Postman 

https://firebasestorage.googleapis.com/v0/b/imagefunctions.appspot.com/o/Postman%20Reader.PNG?alt=media&token=be2d1819-8fa9-41a6-9149-d02d57708796

### Todos

 - Add test base in the response from the endpoint and the database
 - create a function to handle when for some reason some row fail and response with an array named rows failed[] 

License
----

MIT


**Free Software, Hell Yeah!**


